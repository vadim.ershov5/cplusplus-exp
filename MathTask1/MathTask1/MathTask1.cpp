// MathTask1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

double Formula (int x, int n);

int _tmain(int argc, _TCHAR* argv[])
{
	int x,n;
	cout<<"Let's calculate the formula: sinx+sin^(2)x+...+sin^(n)x;"<<endl;
	cout<<"Insert x = ";
	cin>>x;
	cout<<"Insert n = ";
	cin>>n;
	
	cout<<"Result = "<<Formula (x,n)<<endl;

	return 0;
}

double Formula (int x, int n){
	double sum=0;
	for (int i=1; i<=n; i++)
			sum += pow(sin(x),i);
	return (sum);
}