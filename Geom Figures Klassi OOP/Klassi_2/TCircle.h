

#include "TFigure.h"
#include "TCoord.h"

namespace circle
{

class TCircle: public figure::TFigure {
	
	coord::TCoord circleCenter;
	float radius;

public:

	TCircle (float radious, coord::TCoord circleCenter)
	{
	this->radius = radious;
	this->circleCenter = circleCenter;
	};

	TCircle (coord::TCoord circleCenter)
	{
	radius = 5;
	this->circleCenter = circleCenter;
	};

	TCircle ()
	{
		radius = 5;
		coord::TCoord circleCenter;
	};

	void setRadius(float val) { radius=val; }
	float getRadius() { return radius; }
	
	void Draw(coord::TCoord &circle_senter);
	float Diametr();
	double Perimetr();
	double Square();
};

}