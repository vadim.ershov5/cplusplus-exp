#include "TFigure.h"

namespace square
{

class TSquare: public figure::TFigure {

	static const int SIDES_QTY = 4;

public:

	TSquare(float *s): TFigure(s, SIDES_QTY) {};

	void Check(float *s,bool &flag);
	void Draw(coord::TCoord &start_point,float *s);
	float CalcPerimetr(float *s);
	float CalcSquare(float &s);
	double CalcDiagonal(float &s);

};

}