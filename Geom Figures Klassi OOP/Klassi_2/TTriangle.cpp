#include "stdafx.h"
#include "TTriangle.h"

namespace triangle
{

void TTriangle::Draw(coord::TCoord &start_point,float *s) {
	cout<<"Draw Triangle..."<<endl;
	cout<<"Start point: "<<"{"<<start_point.getX()<<";"<<start_point.getY()<<"}"<<endl;

	for(int i=0; i<SIDES_QTY; i++) {
		cout<<"side "<<(i+1)<<":"<<s[i]<<endl;
	}
}

void TTriangle::Check(float *s, bool &flag) {
	if ((s[2] >= s[0] + s[1]) || (s[1] >= s[0] + s[2]) || (s[0] >= s[1] + s[2])) {
		cout<<"This Triangle does not EXIST!"<<endl;
		flag = false;
	}
	else {
		cout<<"This Triangle EXIST!"<<endl;
		flag = true;
	}
}

float TTriangle::CalcPerimetr(float *s){
	float perimetr = 0.0;
	for(int i=0; i<SIDES_QTY; i++) {
		perimetr+=s[i];
	}
	return perimetr;
	//Perim=a+b+c; 
}

float TTriangle::CalcHalfPerimetr(float *s){
	float perimetr = 0.0;
	for(int i=0; i<SIDES_QTY; i++) {
		perimetr+=s[i];
	}
	return perimetr/2;
}

float TTriangle::CalcSquare(float hp,float *s) {
	//formula gerona
    return sqrt(hp * (hp - s[0]) * (hp - s[1]) * (hp - s[2]));
}

float TTriangle::CalcBisect(float *s) {
	return (sqrt(s[0] * s[1] * (s[0] + s[1] + s[2]) * (s[0] + s[1] - s[2]))) / (s[0] + s[1]);
	//cherez 3 storoni
}

float TTriangle::CalcHeight(float p, float *s, float &ms) {
	return (2 * sqrt(p * (p - s[0])*(p - s[1])*(p - s[2])))/ms;
}

}