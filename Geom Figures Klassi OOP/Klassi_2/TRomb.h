
#include "TFigure.h"

namespace romb
{

class TRomb: public figure::TFigure {

	static const int SIDES_QTY = 4;
	double angle_sharp;
	double angle_blunt;
public:

	TRomb(float *s): TFigure(s, SIDES_QTY) {
	angle_sharp = 40;
	angle_blunt = 120;
	};

	TRomb(float *s, double angSh, double angBl): TFigure(s, SIDES_QTY) {
		this ->angle_sharp = angSh;
		this ->angle_blunt = angBl;
	};

	void setAngleSH(double val) { angle_sharp = val; }
	void setAngleBL(double val) { angle_blunt = val; }
	double getAngleSH() { return angle_sharp; }
	double getAnggleBL() { return angle_blunt; }

	void Check(float *s,bool &flag);
	void Draw(coord::TCoord &start_point,float *s);
	float CalcPerimetr(float &s);
	double CalcSquare(float &s, double h);
	double CalcShortDiagonal(float &s);
	double CalcLongDiagonal(float &s);
	double CalcHeight(float &s);

};

}