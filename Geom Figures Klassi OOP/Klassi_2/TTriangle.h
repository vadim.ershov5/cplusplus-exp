#include "TFigure.h"

namespace triangle
{

class TTriangle:figure::TFigure {

	static const int SIDES_QTY = 3;

public:
	TTriangle(float *s): TFigure(s, SIDES_QTY) {};

	void Check(float *s, bool &flag);
	void Draw(coord::TCoord &start_point,float *s);
	float CalcPerimetr(float *s);
	float CalcHalfPerimetr(float *s);
	float CalcSquare(float hp,float *s);
	float CalcBisect(float *s);
	float CalcHeight(float p, float *s, float &ms);
};

}
