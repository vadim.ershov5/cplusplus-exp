#include "stdafx.h"
#include "TParallelogram.h"

namespace prlgr
{

void TParallelogram::Draw(coord::TCoord &start_point,float *s) {
	cout<<"Draw Parallelogram..."<<endl;
	cout<<"Start point: "<<"{"<<start_point.getX()<<";"<<start_point.getY()<<"}"<<endl;

	for(int i=0; i<SIDES_QTY; i++) {
		cout<<"side "<<(i+1)<<":"<<s[i]<<endl;
	}

	cout<<"A sharp angle at the base(degrees): "<<getAngle()<<endl;

}

void TParallelogram::Check(float *s, bool &flag) {
	if ((s[0]==s[2]) & (s[1]==s[3])) {
		cout<<"This is Parallelogram!"<<endl;
		flag = true;
	}
	else {
		cout<<"This is NOT Parallelogram!"<<endl;
		flag = false;
	}
}

float TParallelogram::CalcPerimetr(float *s){
	return 2 * (s[0]+s[1]);
	//int Perim=a+b+c+d;
}

double TParallelogram::CalcSquare(float *s, double h) {
	double x = 0.0;
	if (s[0] < s[1])
		x = s[0];
	else
		x = s[1];

	return h * x;
}

double TParallelogram::CalcShortDiagonal(float *s) {
	double y = 0.0;
	
	y = angle_sharp / 180.0 * M_PI; //from degrees to radian

	return sqrt(pow(s[0],2) + pow(s[1],2) - 2*s[0]*s[1] * cos(y));
}

double TParallelogram::CalcLongDiagonal(float *s) {
	double y = 0.0;
	
	y = angle_sharp / 180.0 * M_PI; //from degrees to radian

	return sqrt(pow(s[0],2) + pow(s[1],2) - 2*s[0]*s[1] * cos(y));
}


double TParallelogram::CalcHeight(float *s)
{
	double y,z = 0.0;
	y=sin(angle_sharp / 180.0 * M_PI); //from degrees to radian

	if (s[0] < s[1])
		z = s[0];
	else
		z = s[1];

	return z * y;
}

}