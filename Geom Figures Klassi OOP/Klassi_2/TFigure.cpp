#include "stdafx.h"
#include "TFigure.h"

namespace figure
{

TFigure::TFigure(void)
{
	sides_qty=0;
	for(int i=0; i<4; i++)
		sides[i]=0;
}

TFigure::TFigure(float *s, int sides_qty)
{
	this->sides_qty = sides_qty;
	for(int i=0; i<sides_qty; i++)
		sides[i]=s[i];
}

TFigure::~TFigure(void)
{
}

void TFigure::Draw()
{
}

float TFigure::CalcPerimetr()
{
	return 0.0;
}

float TFigure::CalcSquare()
{
	return 0.0;
}

}