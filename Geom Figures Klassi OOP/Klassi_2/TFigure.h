#pragma once // ��� ������, ������ ����� ������� � �������� ������ ������ ���� ���      

#include "TCoord.h"

namespace figure
{

class TFigure {
protected:

	coord::TCoord start_point;
	int sides_qty;
	float sides[4];

public:

	TFigure(void);
	TFigure(float *s, int sides_qty);
	~TFigure(void);

	void Draw();
	float CalcPerimetr();
	float CalcSquare();

};

}