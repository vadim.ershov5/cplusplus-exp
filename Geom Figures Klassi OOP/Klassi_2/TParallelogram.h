
#include "TFigure.h"

namespace prlgr
{

class TParallelogram: public figure::TFigure {

	static const int SIDES_QTY = 4;
	double angle_sharp; //Enter a sharp angle at the base(degrees)

public:

	TParallelogram(float *s): TFigure(s, SIDES_QTY) {
	angle_sharp = 0.0;
	};

	TParallelogram(float *s, double ang): TFigure(s, SIDES_QTY) {
	this -> angle_sharp = ang;
	};
	
	void setAngle(double val) { angle_sharp=val; }
	double getAngle() { return angle_sharp; }

	void Check(float *s,bool &flag);
	void Draw(coord::TCoord &start_point,float *s);
	float CalcPerimetr(float *s);
	double CalcSquare(float *s, double h);
	double CalcShortDiagonal(float *s);
	double CalcLongDiagonal(float *s);
	double CalcHeight(float *s);

};

}