// Klassi_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TFigure.h"
#include "TCoord.h"
#include "TCircle.h"
#include "TSquare.h"
#include "TTriangle.h"
#include "TParallelogram.h"
#include "TRomb.h"


int _tmain(int argc, _TCHAR* argv[])
{
	
	bool flag;
	
	float sides[] = {7,10,5};
	triangle::TTriangle triangle1(sides);
	coord::TCoord start_point(10,15);
	triangle1.Check(sides, flag);
	if (flag) {
		triangle1.Draw(start_point,sides);
		cout<<"Triagle Perimetr: "<<triangle1.CalcPerimetr(sides)<<endl;
		cout<<"Triagle Square: "<<triangle1.CalcSquare(triangle1.CalcHalfPerimetr(sides),sides)<<endl;
		cout<<"Triagle Bisect: "<<triangle1.CalcBisect(sides)<<endl;
		cout<<"Triagle Hight Side1: "<<triangle1.CalcHeight(triangle1.CalcHalfPerimetr(sides), sides, sides[0])<<endl;
		cout<<"Triagle Hight Side2: "<<triangle1.CalcHeight(triangle1.CalcHalfPerimetr(sides), sides, sides[1])<<endl;
		cout<<"Triagle Hight Side3: "<<triangle1.CalcHeight(triangle1.CalcHalfPerimetr(sides), sides, sides[2])<<endl;
	}
	cout<<'\n';

	float sidesSQ1[] = {5,5,5,5};
	square::TSquare square1(sidesSQ1);
	coord::TCoord square1_point;
	square1.Check(sidesSQ1, flag);
	if (flag) {
		square1.Draw(square1_point,sidesSQ1);
		cout<<"Square Perimetr: "<<square1.CalcPerimetr(sidesSQ1)<<endl;
		cout<<"Square Square: "<<square1.CalcSquare(sidesSQ1[0])<<endl;
		cout<<"Square Diagonal: "<<square1.CalcDiagonal(sidesSQ1[0])<<endl;
	}
	cout<<'\n';

	coord::TCoord circle1Center;
	circle1Center.InputFigureCoords(circle1Center);
	circle::TCircle circle1(10, circle1Center);
	circle1.Draw(circle1Center);
	cout<<"Circle Diametr: "<<circle1.Diametr()<<endl;
	cout<<"Circle Perimetr: "<<circle1.Perimetr()<<endl;
	cout<<"Circle Square: "<<circle1.Square()<<endl;
	cout<<'\n';

	float sidesPg1[] = {5,10,5,10};
	coord::TCoord prg1_point(23,10);
	prlgr::TParallelogram prg1(sidesPg1,60);
	prg1.Check(sidesPg1, flag);
	if (flag) {
		prg1.Draw(prg1_point,sidesPg1);
		cout<<"Parallelogram Perimetr: "<<prg1.CalcPerimetr(sidesPg1)<<endl;
		cout<<"Parallelogram Square: "<<prg1.CalcSquare(sidesPg1, prg1.CalcHeight(sidesPg1))<<endl;
		cout<<"Parallelogram Height: "<<prg1.CalcHeight(sidesPg1)<<endl;
		cout<<"Parallelogram ShortDiagonal: "<<prg1.CalcShortDiagonal(sidesPg1)<<endl;
		cout<<"Parallelogram LongDiagonal: "<<prg1.CalcLongDiagonal(sidesPg1)<<endl;
	}
	cout<<'\n';

	float sidesRmb1[] = {10,10,10,10};
	coord::TCoord rmb1_point(33,17);
	romb::TRomb rmb1(sidesRmb1);
	rmb1.Check(sidesRmb1,flag);
	if (flag) {
		rmb1.Draw(rmb1_point, sidesRmb1);
		cout<<"Romb Perimetr: "<<rmb1.CalcPerimetr(sidesRmb1[0])<<endl;
		cout<<"Romb Square: "<<rmb1.CalcSquare(sidesRmb1[0],rmb1.CalcHeight(sidesRmb1[0]))<<endl;
		cout<<"Romb Height: "<<rmb1.CalcHeight(sidesRmb1[0])<<endl;
		cout<<"Romb ShortDiagonal: "<<rmb1.CalcShortDiagonal(sidesRmb1[0])<<endl;
		cout<<"Romb LongDiagonal: "<<rmb1.CalcLongDiagonal(sidesRmb1[0])<<endl;
	}
	cout<<'\n';

	return 0;
}