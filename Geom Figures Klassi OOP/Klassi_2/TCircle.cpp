#include "stdafx.h"
#include "TCircle.h"

namespace circle
{

void TCircle::Draw(coord::TCoord &circle_senter) {
	cout<<"Draw Circle..."<<endl;
	cout<<"Start point: "<<"{"<<circle_senter.getX()<<";"<<circle_senter.getY()<<"}"<<endl;
	
	cout<<"Circle radius: "<<radius<<endl;
}

float TCircle::Diametr()
{
	return 2 * radius;
}

double TCircle::Perimetr() {
	return 2*M_PI*radius;   
}

double TCircle::Square() {
	return M_PI*pow(radius,2);   
}

}
