﻿#include "stdafx.h"
#include "TSquare.h"

namespace square
{

void TSquare::Draw(coord::TCoord &start_point,float *s) {
	cout<<"Draw Square..."<<endl;
	cout<<"Start point: "<<"{"<<start_point.getX()<<";"<<start_point.getY()<<"}"<<endl;

	for(int i=0; i<SIDES_QTY; i++) {
		cout<<"side "<<(i+1)<<":"<<s[i]<<endl;
	}
}

void TSquare::Check(float *s, bool &flag) {
	if ((s[0]==s[1])&(s[1]==s[2])&(s[2]==s[3])&(s[3]==s[0])&(s[0]==s[2])&(s[1]==s[3])&(s[3]==s[1])) {
		cout<<"This is SQUARE!"<<endl;
		flag = true;
	}
	else {
		cout<<"This is NOT SQUARE!"<<endl;
		flag = false;
	}
}

float TSquare::CalcPerimetr(float *s){
	return s[0]+s[1]+s[2]+s[3];
	//int Perim=a+b+c+d;
}

float TSquare::CalcSquare(float &s) {
    return pow(s,2);
	// kvadrat dlini storoni
}

double TSquare::CalcDiagonal(float &s) {
	return s*sqrt(2);
	// d=s*sqrt(2)
}

}