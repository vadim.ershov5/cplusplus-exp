#include "stdafx.h"
#include "TRomb.h"

namespace romb
{

void TRomb::Draw(coord::TCoord &start_point,float *s) {
	cout<<"Draw Romb..."<<endl;
	cout<<"Start point: "<<"{"<<start_point.getX()<<";"<<start_point.getY()<<"}"<<endl;

	for(int i=0; i<SIDES_QTY; i++) {
		cout<<"side "<<(i+1)<<":"<<s[i]<<endl;
	}
}

void TRomb::Check(float *s, bool &flag) {
	if ((s[0]==s[1])&(s[1]==s[2])&(s[2]==s[3])&(s[3]==s[0])&(s[0]==s[2])&(s[1]==s[3])&(s[3]==s[1])&(angle_sharp != angle_blunt)) {
		cout<<"This is ROMB!"<<endl;
		flag = true;
	}
	else {
		cout<<"This is NOT ROMB!"<<endl;
		flag = false;
	}
}

float TRomb::CalcPerimetr(float &s){
	return s * 4;
}

double TRomb::CalcSquare(float &s, double h) {
    return s * h;
}

double TRomb::CalcShortDiagonal(float &s) {
	double y = 0.0;
	
	y = angle_sharp / 180.0 * M_PI; //from degrees to radian

	return s * sqrt(2 - 2 * cos(y));
}

double TRomb::CalcLongDiagonal(float &s) {
	double y = 0.0;
	
	y = angle_sharp / 180.0 * M_PI; //from degrees to radian

	return s * sqrt(2 + 2 * cos(y));
}

double TRomb::CalcHeight(float &s)
{
	double y = 0.0;
	
	y = angle_sharp / 180.0 * M_PI; //from degrees to radian

	return s * sin(y);
}

}