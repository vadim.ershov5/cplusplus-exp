﻿#include "stdafx.h"
#include "TCoord.h"

namespace coord
{

TCoord::TCoord(void)
{
	x=0;
	y=0;
}

TCoord::TCoord(int _x, int _y)
{
	this->x = _x;
	this->y = _y;
}

TCoord::~TCoord(void)
{
}

void TCoord::InputFigureCoords(TCoord &coords)
{
	int x,y;

	cout<<"Enter coordinates for figure: "<<endl;
	cout<<"X: ";
	cin>>x;
	coords.setX(x);

	cout<<"Y: ";
	cin>>y;
	coords.setY(y);

}

}