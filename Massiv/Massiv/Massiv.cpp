// Massiv.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

void EnterMatrix (int** a, const int N);
void PrintMatrix (int** a, const int N);
void ModifyMatrix (int**a, const int N);

int _tmain(int argc, _TCHAR* argv[])
{
	int N=0;

	cout<<"Enter N for NxN matrix size: ";
	cin>>N;

	int **a = new int*[N];
	for(int i=0;i<N;i++)
		a[i]=new int[N];

	EnterMatrix (a,N);
	PrintMatrix (a,N);
	ModifyMatrix (a,N);

	cout << "Modified matrix:" << "\n";
	PrintMatrix (a,N);

	for (int i = 0; i < N; i++)
    delete[]a[i];

    delete[]a;

	return 0;
}

void EnterMatrix (int** a, const int N) {
	cout << "Enter matrix elements:" << endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j<N; j++) {
			cout << "a[" << i << "][" << j <<"] =";
			cin >> a[i][j];
		}
		cout<<endl;
	}
}

void PrintMatrix (int** a, const int N) {
	cout << "Matrix:" << endl;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j<N; j++) {
				cout << a[i][j] << "\t";
			}
			cout << "\n";
		}
		cout << "\n";
}

void ModifyMatrix (int**a, const int N) {
	int b;
	for (int i = 0; i < N; i++)
    {
        b=a[i][i];
    for (int j = 0; j < N; j++)
    {
        a[i][j]+=b;
	}
    }
}