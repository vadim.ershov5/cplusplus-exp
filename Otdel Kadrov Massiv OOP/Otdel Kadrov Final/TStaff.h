#pragma once
#include "TWorker.h"

namespace staff
{

class TStaff {

private:
	worker::TWorker* staff;
	int counter;

public:
	void AddWorker(worker::TWorker& worker); //��������� ������ ������ � staff,��� ��� �������
	void DeleteWorker(int index); //� �������� �����.������� ����� ������ �� ������� � ���������
	int FindWorkerByCode(int key);
	int GetMaxSalary();
	int GetMinSalary();
	float GetMidSalary();
	void UpMidS_workers(float mid_salary);
	void LowMidS_workers(float mid_salary);
	

	worker::TWorker getWorker(int index) { return staff[index]; }
	int getCounter(){ return counter; }

	TStaff();
	~TStaff();

};

}