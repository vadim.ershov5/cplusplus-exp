
#include "TStaff.h"

namespace user_interface
{
  void EnterWorkerInfo(worker::TWorker& worker);
  void PrintWorkerInfo(worker::TWorker& worker);
  void ShowWorkerList(staff::TStaff& staff);

  int MainMenu(staff::TStaff& staff);
}