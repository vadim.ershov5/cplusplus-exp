#include "stdafx.h"
#include "Interface.h"

namespace user_interface
{
void EnterWorkerInfo(worker::TWorker& worker) //������ ������ ��������� �� ������ � �� ������������� �������� ��� �����
{
	char* name = new char[30];
	int code;
	float salary;

	cout<<"Enter worker name: ";
	cin>>name;
	cout<<"Enter worker code: ";
	cin>>code;
	cout<<"Enter worker salary: ";
	cin>>salary;

	worker.set_code(code); //������������� ��� ���� ���������� �� ����� ������� ������, ��� �������� �� ������
	worker.set_salary(salary); //�������� ���������
	worker.set_name(name);

	//worker = TWorker(name, code, salary); //��� � ������� ������������ ��� ��� ��������� ��� ����
}

void PrintWorkerInfo(worker::TWorker& worker)
{
	cout<<"\n""\t"; 
	cout<<"Worker name: "<<worker.get_name()<<"\t";
	cout<<"Worker code: "<<worker.get_code()<<"\t";
	cout<<"Worker salary: "<<worker.get_salary()<<"\t";
	cout<<endl;
}

void ShowWorkerList(staff::TStaff& staff)
{
	cout<<"\n""\t"<<"Workers list: "<<endl;
	if (staff.getCounter()>0)
	{
		for (int i=0; i<staff.getCounter(); i++) {
			PrintWorkerInfo(staff.getWorker(i));
		}
	}
	else
		cout<<"\n""\t"<<"EMPTY"<<endl;
}

int MainMenu(staff::TStaff& staff) {

	int n = 0;

	cout<<"Enter 1 - Add new Worker"<<endl;
	cout<<"Enter 2 - Delete Worker"<<endl;
	cout<<"Enter 3 - Search Worker by code"<<endl;
	cout<<"Enter 4 - View Workers list"<<endl;
	cout<<"Enter 5 - View Max salary worker"<<endl;
	cout<<"Enter 6 - View Min salary worker"<<endl;
	cout<<"Enter 7 - View middle salary"<<endl;
	cout<<"Enter 8 - View Workers with upper middle salary"<<endl;
	cout<<"Enter 9 - View Workers with lower middle salary"<<endl;
	cout<<"Enter 10 - Exit"<<endl;
	cout<<"Your enter: ";

	cin>>n;
	
	switch(n) 
	{
	case 1: {
		worker::TWorker worker;  //���������� ������ ������
		EnterWorkerInfo(worker); //����� ������ �������
		staff.AddWorker(worker); //�������� �������� ������� � �����
		cout<<endl; break;
			}

	case 2: {
		int s_code = 0;
		cout<<"Enter worker code: ";
		cin>>s_code;
		staff.DeleteWorker(staff.FindWorkerByCode(s_code));
		cout<<endl; break;
			}

	case 3: {
		int s_code = 0;
		cout<<"Enter worker code: ";
		cin>>s_code;
		PrintWorkerInfo(staff.getWorker(staff.FindWorkerByCode(s_code)));
		cout<<endl; break;
			}

	case 4: ShowWorkerList(staff); cout<<endl; break;

	case 5: {
		cout<<"MAX salary worker is:"<<endl;
		int index = staff.GetMaxSalary();
		PrintWorkerInfo(staff.getWorker(index));
			 cout<<endl; break;
			}
			
	case 6: {
		cout<<"MIN salary worker is:"<<endl;
		int index = staff.GetMinSalary();
		PrintWorkerInfo(staff.getWorker(index));
		cout<<endl; break;
			}

	case 7: {
		cout<<"MIDDLE salary is:"<<endl;
		cout<<staff.GetMidSalary()<<"\n";
		cout<<endl; break;
			}

	case 8: {
		staff.UpMidS_workers(staff.GetMidSalary());
		cout<<endl; break;
			}

	case 9: {
		staff.LowMidS_workers(staff.GetMidSalary());
		cout<<endl; break;
			}

	case 10: break;

	default:
		cout<<"No such command! Try again."<<endl; break;
	}
	return n;
}

}