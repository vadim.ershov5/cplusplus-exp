#include "stdafx.h"
#include "TStaff.h"
#include "Interface.h"

namespace staff
{

void TStaff::AddWorker(worker::TWorker& worker)
{
	if(staff != NULL)
	{
		worker::TWorker* tmp_staff = new worker::TWorker[counter];
		for (int i=0; i<counter; tmp_staff[i]=staff[i++]);//������� ���� �����������

		staff = new worker::TWorker[counter+1]; //�������� ������ �� +1 �������

		for (int i=0; i<counter; staff[i]=tmp_staff[i++]); //������� �������
		staff[counter]=worker; //����� ������ ������ � ��������� ������ ������ �������
		counter++;

		delete tmp_staff;
	}
	else
	{
		staff = new worker::TWorker[1]; //���� ���� ������,������ 1 ������� � ��������
		staff[0]=worker;
		counter++;
	}
}
	
void TStaff::DeleteWorker(int index)
{
	if(staff != NULL)   //��������� ��� ������ �� ������
	{
		worker::TWorker* tmp_staff = new worker::TWorker[counter];
		for (int i=0; i<counter; tmp_staff[i]=staff[i++]); //������������ ����

			staff = new worker::TWorker[counter-1]; //������ �� 1 ������� ������

		for (int i=0, j=0; i<counter; i++)//������� �������, ����� �� ����� �������� � ������� ������� 2 �������
		{                         //���� ������ �� ������ �������, ������ �� �������
			if(i != index)//�� ������ ��������� ��������
			{
				staff[j++]=tmp_staff[i]; //������ J ����� ��������� ������ ����� �� ���-�� ����������� � ����� ������
			}           //����� ������� ������������������ ��������� �� j �� ����� �����������
		}            //���� �� ���� i,�� ������������� �����������, ���� i ������� ��� �� ����
	counter--;
	delete[] tmp_staff;
	}
}
	
int TStaff::FindWorkerByCode(int key)
{
	int index=-1;

	while (index<counter) {
		if (staff[index].get_code() == key) break;
		index++;
	}

	return index;
}

int TStaff::GetMaxSalary()
{
	int index=0;
	float max_salary = 0;
	 max_salary = staff[0].get_salary();

	 for (int i = 1; i < counter; i++) {
		if (staff[i].get_salary() > max_salary) {
			max_salary=staff[i].get_salary();
			index=i;
	}
 }
	 return index;
}
	
int TStaff::GetMinSalary()
{
	int index=0;
	 float min_salary = 0;
	 min_salary = staff[0].get_salary();

	 for (int i = 1; i < counter; i++) {
		if (staff[i].get_salary() < min_salary) {
			min_salary=staff[i].get_salary();
			index=i;
	}
 }
	 return index;
}
	
float TStaff::GetMidSalary()
{
	float mid_salary = 0;
	float sum=0;
	 for (int i=0; i<counter; i++)
		 sum += staff[i].get_salary();
	 mid_salary = sum/counter;
	 return mid_salary;
}

void TStaff::UpMidS_workers(float mid_salary)
{
	int c_workers = 0;
	cout<<"\n""\t"<<"Upper middle salary Workers list: "<<endl; 
	for (int i=0; i<counter; i++) {
		 if (staff[i].get_salary() > mid_salary) {
			user_interface::PrintWorkerInfo(staff[i]);
			 cout<<endl;
			 c_workers++;
		 }
	}
	cout<<"\n""\t"<<"Upper middle salary Workers Amount: "<<c_workers<<endl;
}

void TStaff::LowMidS_workers(float mid_salary)
{
	int c_workers = 0; 
	cout<<"\n""\t"<<"Lower middle salary Workers list: "<<endl; 
	 for (int i=0; i<counter; i++) {
		 if (staff[i].get_salary() < mid_salary) {
			 user_interface::PrintWorkerInfo(staff[i]);
			 cout<<endl;
			 c_workers++;
		 }
	 }
	 cout<<"\n""\t"<<"Lower middle salary Workers Amount: "<<c_workers<<endl;

}

TStaff::TStaff()
{
	counter=0;
	staff=NULL;
}
	
TStaff::~TStaff()
{
	if(staff!=NULL) delete[]staff;
}

}