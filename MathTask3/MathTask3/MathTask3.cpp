// MathTask3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

float sin3(float x);

int _tmain(int argc, _TCHAR* argv[])
{
	cout.setf(ios::fixed);
	cout.precision(3);

	for (float x=-4.0; x<=4.0; x+=0.1) {
		cout<<"sin(3 x "<<x<<")"<<" = "<<sin3(x)<<endl;
	}
	return 0;
}

float sin3(float x) {

	return(sin(3*x));

}